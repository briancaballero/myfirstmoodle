var wsToken = "3f9638497aa8226b802e3a8c4e6e02ad";

$(document).ready(function() {
    
    var criteria = [];
    criteria.push({
                key: 'deleted',
                value: 0
            });
    $.ajax({
        url: '/webservice/rest/server.php',
        method: "POST",
        data: {
            wstoken: wsToken,
            wsfunction: "core_user_get_users",
            moodlewsrestformat: 'json',
            criteria: criteria
        },
        success: function(data, status) {
            $.each(data.users, function(index, user) {
                var html = '<li><a href="/user/view.php?id=' + user.id + '" title="' + user.fullname + '">' + user.fullname + '</a></li>';
                $("#users_list").append(html);
            });
        }
    });

    var criteria = [];
    criteria.push({
                key: 'category',
                value: 1
            });
    $.ajax({
        url: '/webservice/rest/server.php',
        method: "POST",
        data: {
            wstoken: wsToken,
            wsfunction: "core_course_get_courses",
            moodlewsrestformat: 'json'
        },
        success: function(data, status) {
            if(data.length > 1) {
                $.each(data, function(index, course) {
                    if(course.categoryid > 0) {
                        var html = '<li><a href="/course/view.php?id=' + course.id + '" title="' + course.fullname + '">' + course.fullname + '</a></li>';
                        $("#courses_list").append(html);
                    }
                });
            } else {
                $("#courses_list").append('<li>No Courses Found.</li>');
            }
        }
    });

});