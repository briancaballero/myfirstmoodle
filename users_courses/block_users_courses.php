<?php
class block_users_courses extends block_base
{
    public function init()
    {
        $this->title = get_string('users_courses', 'block_users_courses');
    }
    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.

    public function get_content()
    {
        global $CFG, $DB, $PAGE;

        if ($this->content !== null)
	    {
	      return $this->content;
	    }
	 
	    $this->content         =  new stdClass;
	    
        /********************
         * Users list
         ********************/
	    $users_html  = '<div class="users">';
        $users_html .= '<h4>Users</h4>';
        $users_html .= '<ul id="users_list">';
        $users_html .= '<ul>';
        $users_html .= '</div>';

        /********************
         * Courses list
         ********************/
        $courses_html  = '<div class="courses">';
        $courses_html .= '<h4>Courses</h4>';
        $courses_html .= '<ul id="courses_list">';
        $courses_html .= '<ul>';
        $courses_html .= '</div>';

        $this->content->text = $users_html . $courses_html;
	    
	    $this->content->footer = '';

        $PAGE->requires->js('/blocks/users_courses/js/jquery-1.11.2.min.js');
        $PAGE->requires->js('/blocks/users_courses/js/users_courses.js');
	 
	    return $this->content;
	}
}   // Here's the closing bracket for the class definition