### What is this repository for? ###

* Moodle custom block for User and Course list

### How do I get set up? ###

* Go to Administration > Site Administration > Plugins > Web Services > External Services
* Add Custom service name 'users_courses'
* Go to Administration > Site Administration > Plugins > Web Services > Manage Tokens
* Add Token for 'users_courses'
* Copy 'users_courses' folder to your moodle 'blocks' folder
* Copy the generated token to `/js/users_courses.js` line 1.  Assign the value to `wsToken` variable.
* Go to your moodle site and log in as an administrator
* Apply necessary updates for the 'Users and Courses' plugin
* On 'Front page settings', 'Turn editing on'
* Select Users and Courses on the 'Add a Block' option

### Who do I talk to? ###

* Email me @ brian.caballero1204@gmail.com